Future<String> fetchUserOrder() {
  return Future.delayed(const Duration(seconds: 4),() => 'Large Latte');
}

Future<void> printOrderMessage() async{
  print('Awaiting user order ...');
  var order = await fetchUserOrder();
  print('Your order is : $order');
}
void main() async{
  countSecond(4);
  await printOrderMessage();
}

void countSecond(int s){
  for(var i=1;i<=s;i++){
    Future.delayed(Duration(seconds: i),()=>print(i));
  }
}